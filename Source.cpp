#include <iostream>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

int main()
{
	srand(time(NULL));
	list<int> newList;
	for (int i = 0; i < 10; i++)
	{
		newList.push_back(0);
	}
	for (auto i : newList)
	{
		cout << i << " ";
	}
	cout << endl;
	transform(newList.begin(), newList.end(), newList.begin(), [](int& ref)
		{
			return ref + 1;
		});
	for (auto i : newList)
	{
		cout << i << " ";
	}
	cout << endl;
	generate(newList.begin(), newList.end(), []()->int
		{
			return rand() % 10;
		});
	for (auto i : newList)
	{
		cout << i << " ";
	}
	cout << endl;
	newList.erase(remove_if(newList.begin(), newList.end(), [](const int& ref)
		{
			return ref < 4;
		}), newList.end());
	for (auto i : newList)
	{
		cout << i << " ";
	}
	cout << endl;
}